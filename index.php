<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
    </head>
    <body>
        <?php
        /**
         * Constante que define el caracter de separacion de carpetas.
         */
        define('DS', DIRECTORY_SEPARATOR);
        /**
         * 
         */
        /**
         * Constante que define la ruta del directorio dentro del sistema de archivos.
         * 
         * @example /var/www/frameworkPHP/
         */
        /*
        define('FRAMEWORK_ROOT', realpath(dirname(__FILE__)) . DS);
        define('RUTA_CARPETA_APLICACION', FRAMEWORK_ROOT . 'aplicacion' . DS);
        
        require(RUTA_CARPETA_APLICACION."Arranque.php");
        require(RUTA_CARPETA_APLICACION."Peticion.php");
        
        require(RUTA_CARPETA_APLICACION."Modelo.php");
        require(RUTA_CARPETA_APLICACION."Vista.php");
        require(RUTA_CARPETA_APLICACION."Controlador.php");
        
        $boot=new Arranque();
        $boot->inicializar(new Peticion());
        */
        
        include "public/vistas/inicio.php";
        
        ?>
    </body>
</html>
