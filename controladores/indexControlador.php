<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of indexControlador
 *
 * @author Marta
 */
class indexControlador extends Controlador {

    //put your code here

    public function __construct() {
       // echo "</br> Estas en el Controlador Index </br>";
    }

    public function mostrar() {
        $vista=new Vista();
        $vista->renderizar("index/index",array("header"=>true,"footer"=>true));
    }

}
