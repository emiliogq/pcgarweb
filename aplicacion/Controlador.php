<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Controlador
 *
 * @author emiliogq
 */
 class Controlador {
    private $vista;
    /**
     * Metodo index que implementaran obligatoriamente todos los controladores hijos.
     */
    public function getVista() {
        return $this->vista;
    }

    public function setVista($vista) {
        $this->vista = $vista;
    }

        
    public function mostrar(){}
}
