<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Bootstrap
 *
 * @author emiliogq
 */
class Arranque {
    //put your code here
    public static function inicializar($peticion){
        //Cogemos el controlador de la peticion URL del navegador
        $claseControlador=$peticion->getControlador()."Controlador";
        //
        $rutaControlador=FRAMEWORK_ROOT."controladores".DS.$claseControlador.".php";
        
        
        
        //Comprobamos si el archivo es legible 
        if (is_readable($rutaControlador)){
            //Hacemos el require del controlador pasado por la url del navegador.
            require_once $rutaControlador;
            
            //Instanciamos un controlador.
            $controlador=new $claseControlador;
            
            
            if (is_callable(array($controlador,$peticion->getMetodo()))){
                $metodo=$peticion->getMetodo();
            }
            else{
               $metodo=$peticion::METODO_POR_DEFECTO;
            }
            
            //Si hemos pasado parametros por la url
            if (count($peticion->getArgumentos())>0){
                $parametrosMetodo=$peticion->getArgumentos();
                
                //Utilizamos esta funcion de php para usar el objeto instanciado (controlador)
                //pasado por la url y el metodo de este objeto, todo esto se pasa en un array, 
                //y otro array para los parametros del metodo
                call_user_func_array(array($controlador,$metodo), $parametrosMetodo);
            }
            else{
                call_user_func(array($controlador,$metodo));
            }
        }
        else{
            echo "No es una ruta válida.";
        }
    }
}
