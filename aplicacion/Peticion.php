<?php

/**
 * Clase que sirve para gestionar la peticion de la Url
 *
 * @author emiliogq
 */
class Peticion {

    const CONTROLADOR_POR_DEFECTO = "index";
    const METODO_POR_DEFECTO = "mostrar";
 

    private $controlador;
    private $metodo;
    private $argumentos;

    public function __construct() {
        if (isset($_GET["url"])) {
            $url = filter_input(INPUT_GET, "url", FILTER_SANITIZE_URL);
            $url = explode("/", $url);
            $url = array_filter($url);
            
            $this->controlador = strtolower(array_shift($url));
            $this->metodo = strtolower(array_shift($url));
            $this->argumentos = $url;
        }
        
       
        if (!$this->controlador) {
            $this->controlador = self::CONTROLADOR_POR_DEFECTO;
        }
        ;
        if (!$this->metodo) {
            $this->metodo = self::METODO_POR_DEFECTO;
        }

        if (!$this->argumentos) {
            $this->argumentos = array();
        }
    }

    public function getControlador() {
        return $this->controlador;
    }

    public function getMetodo() {
        return $this->metodo;
    }

    public function getArgumentos() {
        return $this->argumentos;
    }

    public function setControlador($controlador) {
        $this->controlador = $controlador;
    }

    public function setMetodo($metodo) {
        $this->metodo = $metodo;
    }

    public function setArgumentos($argumentos) {
        $this->argumentos = $argumentos;
    }

}
