<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Vista
 *
 * @author emiliogq
 */
class Vista {
    public $titulo;
    
    function __construct() {
      $this->titulo="Inicio";  
    }
    
    function renderizar($vista,$incluir=array("header"=>false,"footer"=>false)){
        
        $rutaVista=FRAMEWORK_ROOT ."vistas" . DS . $vista . ".php";
        // echo $rutaVista;
        if (is_readable($rutaVista)){
                
            if ($incluir["header"]===true){
                require FRAMEWORK_ROOT."vistas".DS."header".".php";
            }
            require $rutaVista;
            if ($incluir["footer"]===true){
                require FRAMEWORK_ROOT."vistas".DS."footer".".php";
            }
            
        }
        else{
            //Lanzar error
            echo "La vista no es correcta.";
        }
    }

}
